# silayer tests

In order to run all silayer tests, simply run `pytest` while `silayer/tests` is your
current working directory.

## Dependencies
You will need the following:
* `pytest`
* `bitstring`

You will also have to have `silayer` installed.

In order to install a dependency, from the bash command line try
```bash
pip3 install DEPENDENCY --user
```
Where `DEPENDENCY` is substituted for `pytest` or `bitstring`.
If you are using anaconda, you may be able to drop the `--user` flag.
