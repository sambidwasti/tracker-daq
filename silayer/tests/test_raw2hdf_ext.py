"""Test the raw2hdf python extension.
"""
import random

import numpy as np

from silayer import _raw2hdf

from test_raw2hdf import generate_data_packet


def rand_nbit(nbit: int) -> int:
    """Return a random number given data width, `nbit`."""
    try:
        return np.random.randint(0, (1 << nbit) - 1)
    except ValueError:
        return np.random.randint(0, (1 << nbit) - 1, dtype=np.uint64)


def test_extract_asic_running_times_small(n_asic=12):
    """Test the extract_asic_running_times function.

    Test with small values (< 1000)
    """
    goal = list(np.random.randint(0, 1e3, n_asic))
    ap_data = [{"running_time": test_value} for test_value in goal]
    data = generate_data_packet(
        asic_packet_data=ap_data, n_asic=n_asic, return_bytes=True
    )
    packet = _raw2hdf.bytes2packet(data)
    assert goal == _raw2hdf.extract_asic_running_times(
        packet["asic_data"], packet["asic_nbytes"]
    ), "Failed to extract asic running times."


def test_extract_asic_running_times_large(n_asic=12):
    """Test the extract_asic_running_times function.

    Test with large values (up to 1e12)
    """
    goal = list(np.random.randint(0, 1e12, n_asic))
    ap_data = [{"running_time": test_value} for test_value in goal]
    data = generate_data_packet(
        asic_packet_data=ap_data, n_asic=n_asic, return_bytes=True
    )
    packet = _raw2hdf.bytes2packet(data)
    assert goal == _raw2hdf.extract_asic_running_times(
        packet["asic_data"], packet["asic_nbytes"]
    ), "Failed to extract asic running times."


def random_trigger_status():
    """Return a random trigger status.

    Returns
    -------
    (status_list, value): (List[bool], int)
        `status_list` is a 16-element list of booleans. `value` is the corresponding "u32"
        representation.
    """
    status = [random.choice([True, False]) for _ in range(16)]
    value = 0
    for i, stat in enumerate(status):
        if stat:
            value |= 1 << i
    return status, value


def test_extract_asic_trigger_status(n_asic=12):
    """Test the extract_asic_trigger_status function."""
    ap_data, goals = [], []
    for _ in range(n_asic):
        status, value = random_trigger_status()
        ap_data.append({"trigger_status": value})
        goals.append(status)
    data = generate_data_packet(
        asic_packet_data=ap_data, n_asic=n_asic, return_bytes=True
    )
    packet = _raw2hdf.bytes2packet(data)
    trigger_stats = _raw2hdf.extract_asic_trigger_status(
        packet["asic_data"], packet["asic_nbytes"]
    )
    for i in range(n_asic):
        assert (
            trigger_stats[i] == goals[i]
        ), f"Trigger stat extraction failed on asic {i}"


def test_bytes2packet_asic_packets(n_asic=12):
    """Test that asic packets are parsed correctly when running bytes2packet in extension.
    """
    ap_data, trigger_status = [], []
    for _ in range(n_asic):
        d = {}
        d["event_id"] = rand_nbit(32)
        d["event_number"] = rand_nbit(32)
        d["running_time"] = rand_nbit(64)
        d["live_time"] = rand_nbit(64)
        d["start_bit"] = rand_nbit(1)
        d["asic_data"] = [rand_nbit(10) for _ in range(34)]
        status, d["trigger_status"] = random_trigger_status()
        ap_data.append(d)
        trigger_status.append(status)
    data = generate_data_packet(
        asic_packet_data=ap_data, n_asic=n_asic, return_bytes=True
    )

    packet = _raw2hdf.bytes2packet(data)
    for i in range(n_asic):
        ap = packet["asic_packets"][i]
        for k in ["event_id", "event_number", "running_time", "live_time"]:
            assert ap_data[i][k] == ap[k], f"{k} field parse failed."
        assert (
            ap["channel_data"] == ap_data[i]["asic_data"][1:-1]
        ), "Channel data was not parsed successfully."
        assert (
            ap["trigger_status"] == trigger_status[i]
        ), "Trigger status was not parsed successfully."
