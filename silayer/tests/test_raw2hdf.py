"""Test raw2hdf module.

# To Do
The only hdf5 fields verified are asic data. Other fields have not been checked.
"""

from contextlib import contextmanager
import json
from functools import reduce
from pathlib import Path
from random import randint, choice as rchoice
import tempfile

import bitstring
import h5py
import numpy as np

from silayer.cfg_reg import pos_default_vcfg
from silayer.raw2hdf import (
    AsicPacket, DataPacket, DataPackets, DataRun, bits2bytes, bytes2bits
)

from typing import Any, Callable, Dict, Iterator, List, Optional, Tuple, Union


#################################
# Bit and byte conversion tests #
#################################

def test_bits2bytes(n_test: int = 10, n_bytes: int = 10) -> None:
    """Test bits2bytes <-> bytes2bits converter.

    Parameters
    ----------
    n_test: int
        Number of tests to run.
    n_bytes: int
        For each test, generate a random number of bytes in [1, n_bytes].
    """
    # Generate bytes to test with.
    for _ in range(n_test):
        bytes = reduce(
            lambda x, y: x + y,
            [randint(0, 255).to_bytes(1, "little") for _ in range(randint(1, n_bytes))],
        )
        assert (
            bits2bytes(bytes2bits(bytes)) == bytes
        ), "bit2bytes is not the inverse of bytes2bits"


####################
# AsicPacket tests #
####################

def pack_data_list(
    data: List[int], nbits: int = AsicPacket.ADC_NBITS
) -> bitstring.BitStream:
    """Pack a list of data values into bits of length `nbits` and concatenate.

    Parameters
    ----------
    data: List[int]
        Data to pack together.
    nbits: int
        Number of bits to use for each data value.

    Returns
    -------
    bs: BitStream
        The packed bits, in a `BitStream` container.
    """
    bitstrings = [bitstring.pack(f"uint:{nbits}", n) for n in data][::-1]
    return reduce(lambda x, y: x + y, bitstrings)


def random_channel_data(n_channel: int = 34) -> Tuple[List[int], bitstring.BitStream]:
    """Create the bitstring array of 340 bytes, along with the random true values.

    Parameters
    ----------
    n_channel: int
        Number of data channels to pack together, each with `AsicPacket.ADC_NBITS` width.
        Defaults to 34, as there are "34" data channels: 1 dummy channel, 32 channel data,
        then cm data.

    Returns
    -------
    data, bits: List[uint], bitstring.BitStream
        data is the random data. bits are the packed, concatenated result corresponding to
        the list.
    """
    max_val = 1023  # Max for 10 bit data.
    data = [randint(0, max_val) for _ in range(n_channel)]
    return data, pack_data_list(data, nbits=AsicPacket.ADC_NBITS)


def pack_by_asic_packet_lookup(field: str, value: int) -> bitstring.BitStream:
    """Use `AsicPacket._DATA_LAYOUT to help pack a value into a bitstring.

    Parameters
    ----------
    field: str
        Name of the data field to pack. Corresponds to the names in
        `AsicPacket._DATAlAYOUT`.
    value: int
        Value to pack.

    Returns
    -------
    bs: BitStream
        Value packed into a `BitStream` container.
    """
    nbits = dict(AsicPacket._DATA_LAYOUT)[field]
    return bitstring.pack(f"uint:{nbits}", value)


def generate_asic_data(
    return_bytes: bool = False, **kwargs
) -> Tuple[Dict[str, Any], Union[bytes, AsicPacket]]:
    """Generate random data to pack into an asic packet.

    Parameters
    ----------
    return_bytes: bool
        If `True`, return the bytes corresponding to an asic data packet. Otherwise,
        return an `AsicPacket`.
    kwargs: {str: int}
        Set specific packet data using extra kwargs. The keyword `"asic_data"` is special,
        and if provided should contain a list of asic data values to put into the asic
        data packet. When a keyword is not specified, its corresponding packet value will
        be random.

    Returns
    -------
    data, packet: (dict, bytes or AsicPacket)
        Returns a tuple. First element is a dictionary containing the data that was set in
        the packet. Second element is the packet bytes (if `return_bytes` is `True`), or
        the `AsicPacket`.
    """
    data = {
        "event_id": kwargs.get("event_id", randint(0, (1 << 32) - 1)),
        "event_number": kwargs.get("event_number", randint(0, (1 << 32) - 1)),
        "trigger_status": kwargs.get("trigger_status", randint(0, (1 << 32) - 1)),
        "running_time": kwargs.get("running_time", randint(0, (1 << 64) - 1)),
        "live_time": kwargs.get("live_time", randint(0, (1 << 64) - 1)),
        "start_bit": kwargs.get("start_bit", randint(0, 1)),
        "chip_data_bit": kwargs.get("chip_data_bit", randint(0, 1)),
        "trigger_bit": kwargs.get("trigger_bit", randint(0, 1)),
        "seu_bit": kwargs.get("seu_bit", randint(0, 1)),
        "status_bits": kwargs.get("status_bits", randint(0, (1 << 34) - 1)),
        "stop_bit": kwargs.get("stop_bit", randint(0, 1)),
    }

    bs_dict = {
        field: pack_by_asic_packet_lookup(field, value) for field, value in data.items()
    }

    if kwargs.get("asic_data") is None:
        asic_data, asic_bitstring = random_channel_data()
    else:
        asic_data = kwargs["asic_data"]
        asic_bitstring = pack_data_list(asic_data, nbits=AsicPacket.ADC_NBITS)
    bs_dict["all_channel_data"] = asic_bitstring

    data["dummy_data"] = asic_data[0]
    data["data"] = asic_data[1:-1]
    data["cm_data"] = asic_data[-1]
    data["channel_status"] = [
        int(n) for n in bitstring.pack("uint:34", data["status_bits"])[1:-1].bin[::-1]
    ]
    del data["status_bits"]

    pack_order = [bs_dict[field] for field, _ in AsicPacket._DATA_LAYOUT][::-1]
    bs = reduce(lambda x, y: x + y, pack_order)

    if len(bs) % 8 != 0:
        bits_remain = 8 - (len(bs) % 8)
        bs = bitstring.pack(f"uint:{bits_remain}", 0) + bs

    bytes = bits2bytes([int(n) for n in bs[::-1].bin])

    if return_bytes:
        return data, bytes
    else:
        return data, AsicPacket(bytes)


def test_asic_packet(n_test: int = 10) -> None:
    """Test `AsicPacket` instantiation with bytes, using randomly generated data.

    This uses the bitstring package. We are trusting that they are more reliable than us
    and we could move to use this package in the future.

    Parameters
    ----------
    n_test: int
        Number of tests to run.
    """
    for _ in range(n_test):
        data, ap = generate_asic_data()
        for k in data:
            ap_val = getattr(ap, k)
            assert data[k] == ap_val, (
                f"Data packet attribute was not set correctly. "
                f"packet.{k} = {ap_val}. Should be {data[k]}"
            )


####################
# DataPacket tests #
####################

def generate_data_packet(
    asic_packet_data: Optional[List[Dict[str, Any]]] = None,
    n_asic: int = 12,
    return_bytes: bool = False,
    **kwargs
):
    """Create a data packet.

    Parameters
    ----------
    asic_packet_data: List[Dict[str, Any]] or None
        If asic_data_packet None, use randomly generated data. Otherwise,
        `asic_packet_data` should be a list of dictionaries, where each dict in the list
        will be fed to `generate_asic_data` function as kwargs.
    n_asic: int
        Number of asics in the packet.
    return_bytes: bool
        If `True`, return the data packet bytes. Otherwise, return a `DataPacket` object.
    kwargs: Dict[str, Any]
        Specify extra data attributes to feed into the packet by specifying them with
        kwargs.
    """
    if asic_packet_data is None:
        asic_packet_data = n_asic * [{}]
    assert (
        len(asic_packet_data) == n_asic
    ), "Improper usage of generate_data_packet args."

    asic_data = [
        generate_asic_data(return_bytes=True, **ap_data) for ap_data in asic_packet_data
    ]

    packet_sz = sum(dsize for _, dsize in DataPacket._HEADER_LAYOUT)
    packet_sz += sum(DataPacket._ASIC_NDATA_SZ for _ in range(n_asic))
    packet_sz += sum(len(bytes) for _, bytes in asic_data)

    header_sz = 11  # Read off from data_packet.cpp. Currently stupid.

    # Hard coded data types and ordering that will break if the packet layout changes:
    head_data = [
        bitstring.pack("uint:16", packet_sz),
        bitstring.pack("uint:8", header_sz),
        bitstring.pack(
            "uint:16", kwargs.get("packet_flags", randint(0, (1 << 16) - 1))
        ),
        bitstring.pack(
            "uint:64", kwargs.get("packet_time", randint(0, (1 << 64) - 1))
        ),
        bitstring.pack("uint:8", n_asic),
    ]
    for _, bytes in asic_data:
        head_data.append(bitstring.pack("uint:16", len(bytes)))

    bs = reduce(lambda x, y: x + y, head_data[::-1])
    bytes = bits2bytes([int(n) for n in bs[::-1].bin])

    for _, b in asic_data:
        bytes += b

    if return_bytes:
        return bytes
    else:
        return DataPacket(bytes)


def test_data_packet_channel_data(n_asic: int = 12) -> None:
    """Test: Each asic's channel data should be `channel_number`, from 1 to 32.

    Parameters
    ----------
    n_asic: int
        Number of asics in the data packet to test.
    """
    asic_packet_data = [{"asic_data": list(range(0, 34))} for asic in range(n_asic)]
    dp = generate_data_packet(asic_packet_data=asic_packet_data, n_asic=n_asic)
    for n in range(n_asic):
        ap = dp.asic_packets[n]
        assert ap.dummy_data == 0, "Dummy data incorrectly set in packet."
        assert ap.cm_data == 33, "CM data incorrectly set in packet."
        assert ap.data == list(range(1, 33)), "Channel data incorrectly set in packet"


def test_data_packet_asic_ordering(n_asic: int = 12) -> None:
    """Test: Make sure that asics are in the right order within a packet.

    All channel data should be the asic number.

    Parameters
    ----------
    n_asic: int
        Number of asics in the data packet to test.
    """
    asic_packet_data = [{"asic_data": 34 * [asic]} for asic in range(n_asic)]
    dp = generate_data_packet(asic_packet_data=asic_packet_data, n_asic=n_asic)
    for n in range(n_asic):
        ap = dp.asic_packets[n]
        assert ap.dummy_data == n, "Dummy data incorrectly set in packet."
        assert ap.cm_data == n, "CM data incorrectly set in packet."
        assert ap.data == 32 * [n], "Channel data incorrectly set in packet"


def test_channel_and_asic_ordering(n_asic: int = 12) -> None:
    """Test: set channel data to n_channel*asic + channel, where n_channel is 34.

    Parameters
    ----------
    n_asic: int
        Number of asics in the data packet to test.
    """
    asic_packet_data = [
        {"asic_data": list(range(34 * n, 34 * (n + 1)))} for n in range(n_asic)
    ]
    dp = generate_data_packet(asic_packet_data=asic_packet_data, n_asic=n_asic)
    for n in range(n_asic):
        ap = dp.asic_packets[n]
        assert ap.dummy_data == 34 * n, "Dummy data incorrectly set in packet."
        assert ap.cm_data == 34 * (n + 1) - 1, "CM data incorrectly set in packet."
        assert ap.data == list(
            range(34 * n + 1, 34 * (n + 1) - 1)
        ), "Channel data incorrectly set in packet"


def test_data_packet_time(n_asic: int = 12, n_test: int = 10) -> None:
    """Test that packet time is correctly set.

    Parameters
    ----------
    n_asic: int
        Number of asics in the data packet to test.
    n_test: int
        Number of tests to run.
    """
    for _ in range(n_test):
        packet_time = randint(0, (1 << 64) - 1)
        dp = generate_data_packet(n_asic=n_asic, packet_time=packet_time)
        assert dp.packet_time == packet_time, "Data packet time incorrectly set."


def test_data_packet_to_bytes(n_asic: int = 12, n_test: int = 10) -> None:
    """Test that the `to_bytes` method works with randomly generated data packets.

    Parameters
    ----------
    n_asic: int
        Number of asics in the data packet to test.
    n_test: int
        Number of tests to run.
    """
    for _ in range(n_test):
        bytes = generate_data_packet(n_asic=n_asic, return_bytes=True)
        dp = DataPacket(bytes)
        assert dp.to_bytes() == bytes, "DataPacket.to_bytes conversion is broken."


#####################
# DataPackets tests #
#####################

def data_packets_bytes(
    f: Callable[[int, int, int], int],
    g: Optional[Callable[[int], int]] = None,
    n_asic: int = 12,
    n_packet: int = 10,
) -> bytes:
    """Generate the bytes for a sequence of data packets.

    Parameters
    ----------
    f: f(int, int, int) -> int
        f(i, j, k) -> channel data to specify for the ith packet, jth asic, and kth
        channel. Third argument to `f` will run from 0 to 33. 0 is for dummy data, 33 is
        for the cm data. Asic data channels set with k in 1..32.
    g: g(int) -> dict[str, Any]
        Function for generating packet-specific data. g(i) should produce kwargs for
        generating the ith data packet using the `generate_data_packet` function.
    n_asic: int
        Number of asics per packet.
    n_packet: int
        Number of packets to create.

    Returns
    -------
    packet: bytes
        Packet data as bytes.
    """
    bytes = b""
    for i_packet in range(n_packet):
        if f is None:
            asic_packet_data = None
        else:
            asic_packet_data = [
                {"asic_data": [f(i_packet, j_asic, k_channel) for k_channel in range(34)]}
                for j_asic in range(n_asic)
            ]
        kwargs = {} if g is None else g(i_packet)
        bytes += generate_data_packet(
            asic_packet_data=asic_packet_data, n_asic=n_asic, return_bytes=True, **kwargs
        )
    return bytes


def test_data_packets_from_binary(n_asic: int = 12, n_packet: int = 10) -> None:
    """Test the `from_binary` method to generate data packets.

    This will also probe the packet time, as packet time will be set to packet number.
    Also verifies the data packet array shapes.

    Parameters
    ----------
    n_asic: int
        Number of asics per packet.
    n_packet: int
        Number of packets to create.
    """
    # Simple, generate packet time based on packet number.
    def g(i_packet):
        return {"packet_time": i_packet}

    bytes = data_packets_bytes(None, g=g, n_asic=n_asic, n_packet=n_packet)
    dps = DataPackets.from_binary(bytes, use_c_ext=False)
    for n, dp in enumerate(dps.data_packets):
        assert n == dp.packet_time == dps.packet_time[n], "Packet time incorrectly set."

    assert dps.n_asic == n_asic, "DataPackets.n_asic incorrect"
    assert dps.n_packet == n_packet, "DataPackets.n_packet"
    assert dps.data.shape == (n_asic, n_packet, 32), "DataPackets.data shape incorrect."


def test_data_packets_asic_data(n_asic: int = 12, n_packet: int = 10) -> None:
    """Create data packets, and check that asic data values are being set as they should.

    This should establish that channel data is not getting "shuffled" around.

    Parameters
    ----------
    n_asic: int
        Number of asics per packet.
    n_packet: int
        Number of packets to create.
    """
    # Simple function that depends on packet, asic, and channel.
    # Each channel's data will be unique for a given packet.
    def f(i_packet, j_asic, k_channel):
        return i_packet + 34*j_asic + k_channel

    bytes = data_packets_bytes(f, g=None, n_asic=n_asic, n_packet=n_packet)
    dps = DataPackets.from_binary(bytes, use_c_ext=False)
    for j_asic, arr in enumerate(dps.data):
        for i_packet in range(n_packet):
            # [1: -1] slicing necessary to remove cm, dummy data
            goal = [f(i_packet, j_asic, k_channel) for k_channel in range(34)][1: -1]
            assert list(arr[i_packet]) == goal, "Channel data does not match as it should"

# TO DO: Add more tests of `DataPackets`.


#################
# DataRun tests #
#################

def create_cfg_files(dname: str, n_asic: int = 12) -> None:
    """Generate the config files that live under "*.rdir/configs"

    Uses vanilla config data for now, with random trigger mask and hold delays. Fills in
    the `dname` directory with config data files, but does not return anything.

    Parameters
    ----------
    dname: str
        Path to the directory, under which we will create the "configs" directory.
    n_asic: int
        Number of asics to generate configurations for
    """
    cfg_path = Path(dname) / "configs"
    if not cfg_path.is_dir():
        cfg_path.mkdir(parents=True)

    trigger_mask, hold_delay = {}, {}
    for asic in range(n_asic):
        asic_name = f"asic{asic:02d}"
        pos_default_vcfg.write(str(cfg_path / f"{asic_name}.vcfg"))
        trigger_mask[asic_name] = {
            "tm_hit": rchoice([True, False]),
            "tm_ack": rchoice([True, False]),
            "force_trigger": rchoice([True, False]),
            "asics": [rchoice([True, False]) for _ in range(n_asic)],
        }
        hold_delay[asic_name] = randint(0, 1023)  # Arbitrary limits

    with open(str(cfg_path / "hold-delay.json"), "w") as f:
        json.dump(hold_delay, f)
    with open(str(cfg_path / "trigger-mask.json"), "w") as f:
        json.dump(trigger_mask, f)


@contextmanager
def generate_data_run(
    f: Optional[Callable[[int, int, int], int]] = None,
    n_asic: int = 12,
    n_packet: int = 10,
) -> Iterator[Tuple[str, DataRun]]:
    """Create a data run directory, and write an hdf5 file in the data run directory.

    Run this in a `with` statement, to ensure that the created data run directory and hdf5
    file are cleaned up.

    Parameters
    ----------
    f: Optional, f(int, int, int) -> int
        f is a function of f(i_packet, j_asic, k_channel) -> uint, to populate asic
        channel data. If f is None, then asic data will be random.
    n_asic: int
        Number of asics in the fake data run.
    n_packet: int
        Number of data packetes to produce.

    Returns
    -------
    iterator: yields (str, DataRun)
        Yields only one tuple (as required for use with contextmanager). First element of
        tuple is the hdf5 file path. Second element is a `DataRun` object, used in
        creating the hdf5 file.
    """
    dir = tempfile.TemporaryDirectory()
    dpath = dir.name
    create_cfg_files(dpath, n_asic=n_asic)
    bytes = data_packets_bytes(f, n_asic=n_asic, n_packet=n_packet)
    with open(str(Path(dpath) / "data.rdat"), "wb") as raw_file:
        raw_file.write(bytes)
    dr = DataRun.open(dpath)
    h5path = str(Path(dpath) / "data.h5")
    dr.write_hdf5(h5path)
    try:
        yield h5path, dr
    finally:
        dir.cleanup()


def test_data_run(n_asic: int = 12, n_packet: int = 10) -> None:
    """Test: Check that DataRun and the written hdf5 file are consistent.

    Creates a simple data run with random asic data. Checks that the hdf5 asic data is the
    same as what is in the `DataRun` object.

    Parameters
    ----------
    n_asic: int
        Number of asics in the fake data run.
    n_packet: int
        Number of packets to generate in the data run.
    """
    with generate_data_run(n_asic=n_asic, n_packet=n_packet) as data_run:
        h5path, dr = data_run
        with h5py.File(h5path, "r") as h5:
            for i in range(n_asic):
                group = h5[f"data/asic{i:02d}/data"]
                assert np.all(dr.data[i] == group[...]), (
                    "HDF5 data not consistent with data run"
                )


def test_virtual_channel_data(n_asic: int = 12, n_packet: int = 100) -> None:
    """Test: Check that virtual data matches its corresponding data in data/asicNN/data

    Parameters
    ----------
    n_asic: int
        Number of asics in the fake data run.
    n_packet: int
        Number of packets to generate in the data run.
    """
    n_asic_per_side = 6  # default from raw2hdf.create_virtual_channel_data.
    with generate_data_run(n_asic=n_asic, n_packet=n_packet) as data_run:
        h5path, _ = data_run
        with h5py.File(h5path, "r") as h5:
            assert "vdata" in h5, "vdata group was not created."
            assert "vdata/channel_data" in h5, "vdata/channel_data was not created."
            channel_data = h5["vdata/channel_data"]
            for i_asic in range(n_asic):
                field = f"data/asic{i_asic:02d}/data"
                side = 0 if i_asic < n_asic_per_side else 1
                channel0 = 32 * (i_asic - side * n_asic_per_side)
                assert np.all(
                    channel_data[:, side, channel0:channel0+32] == h5[field][...]
                ), f"Asic {i_asic} virtual data set does not match."
