#include "raw2hdf.h"

struct module_state {
    PyObject *error;
};

static PyMethodDef raw2hdf_methods[] = {
    {"init_parser", init_parser, METH_VARARGS,
        "init_parser(fname): Initialize the data packet parser. Returns a capsule\n"
        "                    Fname is path to data file."},
    {"parse_data_packet", parse_data_packet, METH_VARARGS,
        "parse_data_packets(capsule): Parse a data packet."},
    {"bytes2packet", bytes2packet, METH_VARARGS,
        "bytes2packet(b : bytes) -> dict()"},
    {"extract_channel_data", extract_channel_data, METH_VARARGS,
        "extract_channel_data(b : bytes) -> list()"},
    {"extract_asic_running_times", extract_asic_running_times, METH_VARARGS,
        "extract_asic_running_times(b: bytes, nbytes: list) -> [int] "},
    {"extract_asic_trigger_status", extract_asic_trigger_status, METH_VARARGS,
        "extract_asic_trigger_status(b: bytes, nbytes: list) -> [int] "},
    {NULL, NULL, 0, NULL}
};

static int raw2hdf_traverse(PyObject *m, visitproc visit, void *arg) {
    Py_VISIT(((struct module_state*)PyModule_GetState(m))->error);
    return 0;
}

static int raw2hdf_clear(PyObject *m) {
    Py_CLEAR(((struct module_state*)PyModule_GetState(m))->error);
    return 0;
}

static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "_raw2hdf",
    NULL,
    sizeof(struct module_state),
    raw2hdf_methods,
    NULL,
    raw2hdf_traverse,
    raw2hdf_clear,
    NULL
};

PyMODINIT_FUNC PyInit__raw2hdf(void) {
    PyObject *module = PyModule_Create(&moduledef);
    if (module == NULL)
        return NULL;
    return module;
}

/*     init_parser
 * Currently this will just open a FILE, return a capsule of the FILE pointer.
 */
static PyObject *init_parser(PyObject *dummy, PyObject *args) {
    char *fname; // Does this need to be freed???
    if (!PyArg_ParseTuple(args, "s", &fname)) {
        fprintf(stderr, "Error: usage: init_parser(fname)\n");
        return NULL;
    }
    FileInfo *finfo = (FileInfo *)malloc(sizeof(FileInfo));
    finfo->fp = fopen(fname, "r");
    if (finfo->fp == NULL) {
        fprintf(stderr, "Error: fopen failed on file %s\n", fname);
        return NULL;
    }
    fseek(finfo->fp, 0, SEEK_END);
    finfo->fsz = ftell(finfo->fp);
    fseek(finfo->fp, 0, SEEK_SET);
    return PyCapsule_New((void *)finfo, CAPSULE_NAME, &destroy_capsule);
}

/*     parse_data_packet
 * Return a dictionary for the given data packet.
 * Asic data returned as bytes. I think.
 */
static PyObject *parse_data_packet(PyObject *dummy, PyObject *args) {
    PyObject *capsule;
    if (!PyArg_ParseTuple(args, "O", &capsule)) {
        fprintf(stderr, "Error: usage: parse_data_packet(capsule)\n");
        return NULL;
    }
    FileInfo *finfo = PyCapsule_GetPointer(capsule, CAPSULE_NAME);
    FILE *fp = finfo->fp;
    if (feof(fp) != 0) {
        return PyDict_New();
    }
    DataPacket dp;
    fread(&(dp.packet_size), sizeof(u16), 1, fp);
    if ((long int)dp.packet_size + ftell(fp) - sizeof(u16) > finfo->fsz) {
        // Not enough data remains in the file.
        return PyDict_New();
    }
    fread(&(dp.header_size), sizeof(u8), 1, fp);
    fread(&(dp.packet_flags), sizeof(u16), 1, fp);
    fread(&(dp.packet_time), sizeof(u64), 1, fp);
    fread(&(dp.nasic), sizeof(u8), 1, fp);
    //printf("XXX dp.nasic: %u\n", dp.nasic);
    dp.asic_nbytes = (u16 *)malloc(dp.nasic * sizeof(u16));
    fread(dp.asic_nbytes, sizeof(u16), dp.nasic, fp);
    dp.total_asic_sz = 0;
    for(u8 i=0; i<dp.nasic; i++) {
        //printf("   asic %u: %u nbytes\n", i, dp.asic_nbytes[i]);
        //if (dp.asic_nbytes[i] > 0)
        //    dp.asic_nbytes[i]--; // XXX KLUDGE!!!! THIS SUCKS!!!!
        dp.total_asic_sz += (u32)dp.asic_nbytes[i];
    }
    dp.asic_data = malloc(sizeof(u8) * dp.total_asic_sz);
    //printf("    total_asic_sz: %u\n", dp.total_asic_sz);
    fread(dp.asic_data, sizeof(u8), dp.total_asic_sz, fp);
    PyObject *dp_dict = dp2dict(&dp);

    free(dp.asic_nbytes);
    // XXX Unusre if PyBytes_FromStringAndSize does a copy so that this is safe:
    free(dp.asic_data);
    return dp_dict;
}

static PyObject *bytes2packet(PyObject *dummy, PyObject *args) {
    PyObject *py_bytes;
    if (!PyArg_ParseTuple(args, "O", &py_bytes)) {
        fprintf(stderr, "Error: usage: bytes2packet(bytes)\n");
        return NULL;
    }
    char *data = PyBytes_AsString(py_bytes);

    DataPacket dp;
    memcpy(&(dp.packet_size), data, sizeof(u16)); data += sizeof(u16);
    memcpy(&(dp.header_size), data, sizeof(u8)); data += sizeof(u8);
    memcpy(&(dp.packet_flags), data, sizeof(u16)); data += sizeof(u16);
    memcpy(&(dp.packet_time), data, sizeof(u64)); data += sizeof(u64);
    memcpy(&(dp.nasic), data, sizeof(u8)); data += sizeof(u8);
    dp.asic_nbytes = (u16 *)malloc(dp.nasic * sizeof(u16));
    memcpy(dp.asic_nbytes, data, dp.nasic * sizeof(u16)); data += dp.nasic * sizeof(u16);
    dp.total_asic_sz = 0;
    for (u8 i=0; i<dp.nasic; i++) {
        dp.total_asic_sz += (u32)dp.asic_nbytes[i];
    }
    dp.asic_data = malloc(dp.total_asic_sz * sizeof(u8));
    memcpy(dp.asic_data, data, dp.total_asic_sz * sizeof(u8));
    PyObject *dp_dict = dp2dict(&dp);

    // Get asic packets...
    PyObject *asic_packets_py = PyList_New(dp.nasic);
    for (int i=0; i<(int)dp.nasic; i++) {
        PyList_SetItem(asic_packets_py, i, ap2dict(&dp, i));
    }
    PyDict_SetItemString(dp_dict, "asic_packets", asic_packets_py);

    // Free malloc'd:
    free(dp.asic_nbytes);
    free(dp.asic_data);
    return dp_dict;
}

/*     dp2dict
 * function that turns DataPacket struct into a PyObject dictionary...
 */
PyObject *dp2dict(DataPacket *dp) {
    PyObject *dict = PyDict_New();
    PyDict_SetItemString(dict, "packet_size", PyLong_FromUnsignedLong((unsigned long)dp->packet_size));
    PyDict_SetItemString(dict, "header_size", PyLong_FromUnsignedLong((unsigned long)dp->header_size));
    PyDict_SetItemString(dict, "packet_flags", PyLong_FromUnsignedLong((unsigned long)dp->packet_flags));
    PyDict_SetItemString(dict, "packet_time", PyLong_FromUnsignedLong((unsigned long)dp->packet_time));
    PyDict_SetItemString(dict, "nasic", PyLong_FromUnsignedLong((unsigned long)dp->nasic));

    // XXX HOW DO REFS WORK WITH py_asic_nbytes??? XXX
    PyObject *py_asic_nbytes = PyList_New(dp->nasic);
    for (u8 i=0; i<dp->nasic; i++)
        PyList_SetItem(py_asic_nbytes, i, PyLong_FromUnsignedLong((unsigned long)(dp->asic_nbytes[i])));

    PyDict_SetItemString(dict, "asic_nbytes", py_asic_nbytes);
    PyDict_SetItemString(dict, "asic_data", PyBytes_FromStringAndSize((char *)dp->asic_data, dp->total_asic_sz));

    return dict;
}

/*      ap2dict
 *  Function that turns asic packet data into a PyObject dictionary for a specific asic.
 */
PyObject *ap2dict(DataPacket *dp, int asic) {
    if (asic >= dp->nasic) {
        fprintf(stderr, "E: ap2dict: asic > dp->nasic.\n");
        return NULL;
    }
        
    char *ptr = (char *)dp->asic_data; 
    // Move pointer ahead to where asic of interest's data starts:
    for (int i=0; i<asic; i++)
        ptr += dp->asic_nbytes[i];

    // Extract according to asic packet _DATA_LAYOUT:
    u32 event_id       = *(u32 *)ptr; ptr += sizeof(u32);
    u32 event_number   = *(u32 *)ptr; ptr += sizeof(u32);
    u32 trigger_status = *(u32 *)ptr; ptr += sizeof(u32);
    u64 running_time   = *(u64 *)ptr; ptr += sizeof(u64);
    u64 live_time      = *(u64 *)ptr; ptr += sizeof(u64);

    PyObject *channel_data_py = PyList_New(32); // asic channel data.
    int offset = 1 + 1 + 1 + 1 + 34 + 10; // Ignoring sevaral fields. Extra +10 to get past dummy channel.
    for (int n=0; n<32; n++) {
        int channel = extract_10bit_value(ptr, offset);
        PyList_SetItem(channel_data_py, n, Py_BuildValue("i", channel)); 
        offset += 10;
    }
    
    // Unpack trigger status...
    PyObject *trigger_status_py = PyList_New(16); // 16 trigger bits.
    for (int nbit=0; nbit<16; nbit++)
        PyList_SetItem(trigger_status_py, nbit, PyBool_FromLong((trigger_status >> nbit) & 0x1));
    

    PyObject *dict = PyDict_New();
    PyDict_SetItemString(dict, "event_id", PyLong_FromUnsignedLong(event_id));
    PyDict_SetItemString(dict, "event_number", PyLong_FromUnsignedLong(event_number));
    PyDict_SetItemString(dict, "running_time", PyLong_FromUnsignedLong(running_time));
    PyDict_SetItemString(dict, "live_time", PyLong_FromUnsignedLong(live_time));
    PyDict_SetItemString(dict, "channel_data", channel_data_py);
    PyDict_SetItemString(dict, "trigger_status", trigger_status_py);

    return dict;

}

void destroy_capsule(PyObject *capsule) {
    FileInfo *finfo = PyCapsule_GetPointer(capsule, CAPSULE_NAME);
    fclose(finfo->fp);
    free(finfo);
}

/* asic data packet layout:
_DATA_LAYOUT = [
    ("event_id", 32),
    ("event_number", 32),
    ("trigger_status", 32),
    ("running_time", 64),
    ("live_time", 64),
    ("start_bit", 1),
    ("chip_data_bit", 1),
    ("trigger_bit", 1),
    ("seu_bit", 1),
    ("status_bits", 34),
    ("all_channel_data", 340),
    ("stop_bit", 1),
]

starting offset: 32 + 32 + 32 + 64 + 64 + 1 + 1 + 1 + 1 + 34 = 262
*/

static PyObject *extract_channel_data(PyObject *self, PyObject *args) {
    PyObject *py_bytes, *py_asic_nbytes;
    if (!PyArg_ParseTuple(args, "OO", &py_bytes, &py_asic_nbytes)) {
        fprintf(stderr, "Error: usage: extract_channel_data(bytes, nbytes)\n");
        return NULL;
    }
    char *data = PyBytes_AsString(py_bytes);

    int nasic = PyList_Size(py_asic_nbytes);
    PyObject *asic_data_py = PyList_New(nasic);

    int offset0 = 272;
    for (int asic=0; asic<nasic; asic++) {
        int offset = offset0;
        PyObject *asic_py = PyList_New(32); // asic channel data.
        for (int i=0; i<32; i++) {
            int channel = (int)extract_10bit_value(data, offset);
            PyList_SetItem(asic_py, i, Py_BuildValue("i", channel)); 
            offset = offset + 10;
        }
        PyList_SetItem(asic_data_py, asic, asic_py);
        offset0 = offset0 + 8*PyLong_AsLong(PyList_GetItem(py_asic_nbytes, asic));
    }
    return asic_data_py;
}


static PyObject *extract_asic_running_times(PyObject *self, PyObject *args) {
    PyObject *py_bytes, *py_asic_nbytes;
    if (!PyArg_ParseTuple(args, "OO", &py_bytes, &py_asic_nbytes)) {
        fprintf(stderr, "Error: usage: extract_asic_running_times(bytes, nbytes)\n");
        return NULL;
    }
    char *data = PyBytes_AsString(py_bytes);
    int nasic = PyList_Size(py_asic_nbytes);
    PyObject *running_times_py = PyList_New(nasic);

    int offset = (32 + 32 + 32) / 8;  // Offset to the first running time.
    for (int asic=0; asic<nasic; asic++) {
        char *ptr = data + offset;  // Running time location.
        u64 running_time = *(u64 *)ptr; 
        PyList_SetItem(running_times_py, asic, Py_BuildValue("K", running_time));
        offset = offset + PyLong_AsLong(PyList_GetItem(py_asic_nbytes, asic));
    }
    return running_times_py;
}


static PyObject *extract_asic_trigger_status(PyObject *self, PyObject *args) {
    PyObject *py_bytes, *py_asic_nbytes;
    if (!PyArg_ParseTuple(args, "OO", &py_bytes, &py_asic_nbytes)) {
        fprintf(stderr, "Error: usage: extract_asic_trigger_status(bytes, nbytes)\n");
        return NULL;
    }
    char *data = PyBytes_AsString(py_bytes);
    int nasic = PyList_Size(py_asic_nbytes);
    PyObject *trigger_status_py = PyList_New(nasic);
    int offset = (32 + 32) / 8; // Offset to the first trigger status.
    for (int asic=0; asic<nasic; asic++) {
        char *ptr = data + offset;
        u32 trigger_status = *(u32 *)ptr;
        PyObject *asic_trigger_status_py = PyList_New(16);
        for (int nbit=0; nbit<16; nbit++) {
            PyList_SetItem(asic_trigger_status_py, nbit, PyBool_FromLong((trigger_status >> nbit) & 0x1));
        }
        PyList_SetItem(trigger_status_py, asic, asic_trigger_status_py);
        offset = offset + PyLong_AsLong(PyList_GetItem(py_asic_nbytes, asic));
    }
    return trigger_status_py;
}

/*  extract_10bit_value
 * Given a buffer of data, extract 10 bits, starting at `offset` bit number.
 * Returns as an unsigned short integer.
 * This is used to get the channel data from the asic packet.
 * Note that (offset % 8 != 7), otherwise we might span 3 bytes!!!
 */
u16 extract_10bit_value(char *data, int offset) {
    int byte_start = offset / 8;
    int bit_start = offset % 8;
    u16 val = *(u16 *)(data + byte_start);
    return (val >> bit_start) & 0x3FF;
}

// vim: set ts=4 sw=4 sts=4 et:
