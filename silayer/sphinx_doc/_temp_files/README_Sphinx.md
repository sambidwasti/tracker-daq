# The DAQ S<s>i</s>layer

## The package and its components
`silayer` is a python package containing code for interacting
with the silayer server, and dealing with data sent to/from
a silayer's zynq.

`silayer.client` provides an API to the si layer server.

`silayer.raw2hdf` is a data parsing tool. This module
was initially written with a focus on converting the raw binary
data packets emitted from the silayer zynq to hdf5 files, hence
the name.

`silayer.cfg_reg`: The `cfg_reg` is a self-contained module for writing/reading
VATA 460.3 config registers.

`silayer.data_recvr`: `data_recvr` exists to run a thread dedicated to receiving raw
data from the silayer data emitter, and writing to a flat binary
file. 


## Installation ##
From within the repo root directory (the one containing this file), run:
```bash
pip3 install silayer -r ./silayer/requirements.txt
```

## Documentation Style

### Code
Of course abide by [PEP 8](https://www.python.org/dev/peps/pep-0008/), with the exception
of maximum line lengths. We are using 90 characters as our maximum line length. For bonus
points, it is recommended that code is formatted with
[python black](https://pypi.org/project/black/).


### Docstrings
We are attempting to adhere to the
[numpy docstring guide](https://numpydoc.readthedocs.io/en/latest/format.html) or 
[google docstring guide](https://google.github.io/styleguide/pyguide.html)

The differences can be seen here:
[google doc string](https://bwanamarko.alwaysdata.net/napoleon/format_exception.html)


## Sphinx Documentation
For the ease of working, various executable scripts (for each steps)
have been created. Execute these scripts in order. Some modification
might be needed but ideally, it should not. 

### Steps and Description      

**Running basic scripts**    

There are 3 scripts and you can run these in order. The ```00-sphinx``` script has all 3 scripts in one.
If you are just adding a script, quick edits, this should be good enough. 

* 01-sphinx: Initializes sphinx-apidoc
  * With the full build (F) module is not created. So we rerun the command twice. 
  Once with the F and once without it.
  
* 02-sphinx: Copies, replaces and removes the config and index files. 
  
* 03-sphinx: Creates the html. (by using ``` make html ``` )
  * The folder is in /_build/html.
  * Errors/Warnings with sphinx will be displayed here. 
  * Click on the index.html and verify documentation is ok.    
        


* Few Debugs:       
  
  * If it can't find the file, it's usually an issue with the path. The path is defined in the conf.py
  * Any file with some issue will not be documented.
  * Also check the modules or respective module's ```*.rst``` file to verify the path.
  * If the documentation is not correct, there might be some issue
    with spaces and tabs. Sphinx is really picky. So usually, i just
    copy-paste the working section and edit it rather than trying to 
    figure out what exactly is the issue.
  * After every edit, re-run 03-sphinx. When you refresh the page
    you should see the changes. 

### Gitlab pages

For the gitlab, nothing needs to be done. The ```.gitlab-ci.yml``` already has the command needed there.
The CI/CD pipeline named ```pages``` should take care of auto-documentation.        

**NOTE:If you do not see your script, usually there is some issue with the script which does not 
necessarily provide an issue in the CI/CD pipeline.**
