# silayer/bin directory

Any files that are located here, and are executable, will be installed by pip install.

## NOTE
These files, when installed, may not be "editable" when using pip -e, as they might
actually be copied to a location in $PATH
